import unittest
import edgar_cleaner
import codecs
from bs4 import BeautifulSoup as bs
import regex as re


def extract_from_html(file_path):
    
    page_source = codecs.open(file_path, "r", "utf-8")

    soup = bs(page_source, 'html.parser')
    
    pattern = r'[\\\\ý__•\u2011]'

    mod_string = re.sub(pattern, '', soup.text)
    
    return(mod_string)



class Test_edgar_cleaner(unittest.TestCase):
	
	def test_extract_from_html(self):
		result = extract_from_html(r'C:\Users\EmmaHurley\OneDrive - Kubrick Group\Documents\EDGAR Package\Emma_mods\tests\cleaner_test_html.txt')
		self.assertEqual(result, 'Yay the html cleaner works!')