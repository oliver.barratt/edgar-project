import unittest
import requests
from bs4 import BeautifulSoup
#import edgar_downloader

#give it an incorrect url / filepath and then it will fail 
#raise an exception that either is incorrect 

def check_file_present(file_path):
	import glob, os
	os.chdir(file_path)

	for file in glob.glob("*.html"):
		actual_result = os.path.isfile(file)
	return actual_result

def write_page(url, file_path):
	user_agent='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36'
	r=requests.get(url, headers={'User-Agent':user_agent})
	text = BeautifulSoup(r.text)
	textinput=str(text)
	with open(file_path, "w", encoding="utf-8") as f:
		f.write(textinput)


class Test_edgar_downloader(unittest.TestCase):
	
	def test_write_page(self):
		expected_result = True 

		url = r'https://www.sec.gov/Archives/edgar/data/320193/000032019321000105/aapl-20210925.htm'
		file_path = r'C:\Users\EmmaHurley\OneDrive - Kubrick Group\Documents\EDGAR Package\Emma_mods\tests\Downloader Test\apricots.html'

		write_page(url, file_path)
		
		#actual_result = check_file_present(file_path)
		
		self.assertEqual(expected_result, check_file_present(r'C:\Users\EmmaHurley\OneDrive - Kubrick Group\Documents\EDGAR Package\Emma_mods\tests\Downloader Test'))

