INTRODUCTION
- The Edgar Package includes a set of modules that:
	- Downloads all the 10-K filings of all S&P100 companies and returns it in string format by cleaning the original html format 
	- Converts the Loughran-McDonald Sentiment Dictionary into a Dictionary with Keys being sentiments and Values being the words attached to these
	- Uses this to Create a count of the varying sentiment of words within the 10-K filings
	- Downloads a list of the tickers of all S&P100 companies 
	- Downloads the share price data of all S&P100 companies
- This, in turn, generates the underlying data for sentiment analysis to be conducted in order to discern the effect of the sentiments within 10-K filings on short term share price movements
- Please find instructions below 

INSTRUCTIONS
- Import all modules from Edgar Package.

	Download all html files
- From ref_data import get_sp100.
- Run tickers=get_sp100() # This saves all sp100 tickers to the variable tickers.
- Import function write_page, cik_finder, zeroconcat, download_files_10k from edgar_downloader.
- Run download_all_10k(<file path>) from edgar_downloader. # Provide a file path to save all htmls.

	Convert html into text files
- Import write_n_clean from edgar_cleaner.
- Import extract_from_html. 
- Run write_n_clean(<file_path>, <file_dest>) # File_path is folder where htmls are stored. File dest is where the text is stored.

	Gathering reference data
Yahoo financial data:
- From ref_data import get_sp100.
- Run tickers=get_sp100() # This saves all sp100 tickers to the variable tickers.
- From ref_data import get_yahoo_data
- Google "My User Agent" and Input the result into User_agent variable
- Run  get_yahoo_data('start date', 'end date', tickers, 'interval')
- Note: Dates must be in 'YYYY-MM-DD' format and interval inputs include 'Daily', 'Weekly', 'Monthly'

Sentiment word dictionary
- Import get_sentiment_word_dict from ref_data.
- x = get_sentiment_word_dict()
- This creates a dictionary with Keys being sentiments and Values being words associated with these sentiments, and assigns this dictionary to x

Counting Word Sentiments within 10-k filings
- From Edgar import edgar_sentiment_word_count
- Run write_document_sentiments(<file_path to folder containing 10-K text files>, <file path to a csv file>)

